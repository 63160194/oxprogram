/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.oxprogram2;

import java.util.Scanner;

/**
 *
 * @author OS
 */
class Example {

    static int add(int a, int b) {
        return a + b;
    }

    static String chup(char player1, char player2) {
        if (player1 == 's' && player2 == 'p') {
            return "p1";
        } else if (player1 == 'h' && player2 == 's') {
            return "p1";
        } else if (player1 == 'p' && player2 == 'h') {
            return "p1";
        } else if (player1 == 'h' && player2 == 'p') {
            return "p2";
        } else if (player1 == 'p' && player2 == 's') {
            return "p2";
        } else if (player1 == 's' && player2 == 'h') {
            return "p2";
        }
        return "draw";
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input player 1 (p, h, s):");
        String player1 = sc.next();
        System.out.print("Please input player 2 (p, h, s):");
        String player2 = sc.next();
        String winner = chup(player1.charAt(0), player2.charAt(0));
        System.out.println("Winner is " + winner);
    }

}
